<?php

namespace Application\Model;

class Coin
{
    public $id;
    public $title;
    public $value;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->title = !empty($data['title']) ? $data['title'] : null;
        $this->value = !empty($data['value']) ? $data['value'] : null;
    }
}