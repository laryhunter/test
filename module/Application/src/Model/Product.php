<?php

namespace Application\Model;

class Product
{
    public $id;
    public $title;
    public $price;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['id']) ? $data['id'] : null;
        $this->title = !empty($data['title']) ? $data['title'] : null;
        $this->price = !empty($data['price']) ? $data['price'] : null;
    }
}