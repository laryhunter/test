<?php
namespace Application\Classes\CashBackStrategy;

use Application\Classes\CashBackStrategyInterface;

class IssueLarge implements CashBackStrategyInterface
{
    public function toIssue(array $coins, int $value)
    {
        $coins = array_filter($coins, function ($coin) use ($value) {
            return $coin <= $value;
        });
    
        sort($coins);
        
        $money = [];
        
        $coin = array_pop($coins);
        
        while($value) {
            if($value - $coin < 0) {
                $coin = array_pop($coins);
                continue;
            }
            
            $money[] = $coin;
            $value -= $coin;
        }
        
        return $money;
    }
}