<?php
namespace Application\Classes;

class Cashbox
{
    private $coins = [];
    
    public function __construct(array $coins)
    {
        $this->coins = $coins;
    }
    
    public function pay(\Application\Model\Product $product, \Application\Classes\BillAcceptor $billAcceptor, \Application\Classes\CashBackStrategyInterface $strategy)
    {
        return $strategy->toIssue($this->coins, $billAcceptor->getMoney() - $product->price);
    }
}