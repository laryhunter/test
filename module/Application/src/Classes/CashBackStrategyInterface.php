<?php
namespace Application\Classes;

interface CashBackStrategyInterface
{    
    public function toIssue(array $coins, int $value);
}