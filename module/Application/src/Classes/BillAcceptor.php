<?php

namespace Application\Classes;

class BillAcceptor
{
    private $coins = [];
    
    private $acceptedCoins = [];
    
    private $money = 0;

    public function __construct(array $coins)
    {
        $this->coins = $coins;
    }
    
    public function getMoney()
    {
        return $this->money;
    }
    
    public function getAcceptedCoins()
    {
        return $this->acceptedCoins;
    }
    
    public function add(int $coin)
    {
        if(in_array($coin, $this->coins)) {
            $this->money += $coin;
            $this->acceptedCoins[] = $coin;
        }
    }
}