<?php
namespace Application\Classes;

class CoffeeMachine
{
    private $products = [];
    
    public function __construct(array $products)
    {
        $this->products = $products;
    }
    
    public function toCook(\Application\Model\Product $product, \Application\Classes\BillAcceptor $billAcceptor)
    {
        try {
            $purchased = $this->retrive($product);
        } catch (\Application\Classes\Exception\BadProductException $e) {
            throw $e;
        }
    
        if(!$this->isEnoughMoney($purchased, $billAcceptor->getMoney())) {
            throw new \Application\Classes\Exception\NotEnoughMoneyException();
        }
        
        return $purchased;
    }
    
    private function retrive(\Application\Model\Product $product)
    {
        foreach($this->products as $item) {
            if($item->id == $product->id) {
                return $item;
            }
        }
        
        throw new \Application\Classes\Exception\BadProductException();
    }

    
    private function isEnoughMoney(\Application\Model\Product $product, int $money)
    {
        return $product->price <= $money;
    }
}