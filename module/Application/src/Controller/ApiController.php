<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Application\Classes\BillAcceptor;
use Application\Classes\CoffeeMachine;
use Application\Classes\Cashbox;

class ApiController extends AbstractRestfulController 
{
    const ERR_NOT_FOUND = 100;
    const ERR_NOT_ENOUGH_MONEY = 101;

    private $productTable;
    private $coinTable;
    
    public function __construct(\Application\Model\ProductTable $productTable, \Application\Model\CoinTable $coinTable)
    {
        $this->productTable = $productTable;
        $this->coinTable = $coinTable;
    }
    
    public function getList()
    {
        $products = [];
        foreach ($this->productTable->fetchAll() as $product) {
           $products[] = $product;
        }
    
        return new JsonModel([
            'products' => $products
        ]);
    }
    
    public function create($data)
    {
        try {
            $product = $this->productTable->find($data['id']);
        } catch (\RuntimeException $e) {
            return new JsonModel([
                'success' => false,
                'code' => self::ERR_NOT_FOUND,
                'message' => 'Please select item',
                'coins' => $data['money']
            ]);
        }
        
        $coins = [];
        
        foreach ($this->coinTable->fetchAll() as $coin) {
           $coins[] = $coin->value;
        }
    
        $billAcceptor = new BillAcceptor($coins);
    
        foreach($data['money'] as $coin) {
            $billAcceptor->add((int) $coin);
        }
        
        $products = [];
        foreach ($this->productTable->fetchAll() as $item) {
           $products[] = $item;
        }
        
        $coffeeMachine = new CoffeeMachine($products);
        
        try {
            $purchased = $coffeeMachine->toCook($product, $billAcceptor);
        } catch (\Application\Classes\Exception\BadProductException $e) {
            return new JsonModel([
                'success' => false,
                'code' => self::ERR_NOT_FOUND,
                'message' => 'Please select item',
                'coins' => $billAcceptor->getAcceptedCoins()
            ]);
        } catch (\Application\Classes\Exception\NotEnoughMoneyException $e) {
            return new JsonModel([
                'success' => false,
                'code' => self::ERR_NOT_ENOUGH_MONEY,
                'message' => 'Please deposit cash',
                'coins' => $billAcceptor->getAcceptedCoins()
            ]);
        }
        
        $cashbox = new Cashbox($coins);
        
        return new JsonModel([
            'success' => true,
            'product' => $purchased,
            'cash_back' => $cashbox->pay($purchased, $billAcceptor, new \Application\Classes\CashBackStrategy\IssueLarge())
        ]);
        
    }
}