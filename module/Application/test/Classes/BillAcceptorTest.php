<?php
namespace ApplicationTest\Classes;

use PHPUnit\Framework\TestCase;
use Application\Classes\BillAcceptor;
use Application\Classes\CoffeeMachine;
use Application\Classes\Cashbox;

class CoffeeMachineTest extends TestCase
{
    public function testBillAcceptor()
    {
        $billAcceptor = new BillAcceptor([1, 2, 5, 10, 50, 100]);
        
        $this->assertSame(0, $billAcceptor->getMoney());
        
        $billAcceptor->add(1);
        $this->assertSame(1, $billAcceptor->getMoney());
        $this->assertSame([1], $billAcceptor->getAcceptedCoins());
        
        $billAcceptor->add(10);
        $this->assertSame(11, $billAcceptor->getMoney());
        $this->assertSame([1, 10], $billAcceptor->getAcceptedCoins());
        
        $billAcceptor->add(333);
        $this->assertSame(11, $billAcceptor->getMoney());
        $this->assertSame([1, 10], $billAcceptor->getAcceptedCoins());
        
        $billAcceptor->add(10);
        $this->assertSame(21, $billAcceptor->getMoney());
        $this->assertSame([1, 10, 10], $billAcceptor->getAcceptedCoins());
    }
    
    public function testToCookBadProduct()
    {
        $coffeeMachine = new CoffeeMachine([
            $this->createProduct(1, 'Tea', 100),
            $this->createProduct(1, 'Coffee', 200)
        ]);
        
        $this->expectException(\Application\Classes\Exception\BadProductException::class);
        
        $coffeeMachine->toCook($this->createProduct(3, 'Milk', 100), new BillAcceptor([1, 2, 5, 10, 50, 100]));
    }
    
    public function testToCookNotEnoughMoney()
    {
        $billAcceptor = new BillAcceptor([1, 2, 5, 10, 50, 100]);
        $billAcceptor->add(1);
    
        $coffeeMachine = new CoffeeMachine([
            $this->createProduct(1, 'Tea', 100),
            $this->createProduct(1, 'Coffee', 200)
        ]);
    
        $this->expectException(\Application\Classes\Exception\NotEnoughMoneyException::class);
        
        $coffeeMachine->toCook($this->createProduct(1, 'Tea', 100), $billAcceptor);
    }
    
    public function testToCook()
    {
        $billAcceptor = new BillAcceptor([1, 2, 5, 10, 50, 100]);
        $billAcceptor->add(50);
        $billAcceptor->add(50);
        
        $coffeeMachine = new CoffeeMachine([
            $this->createProduct(1, 'Tea', 100),
            $this->createProduct(1, 'Coffee', 200)
        ]);
        
        $product = $coffeeMachine->toCook($this->createProduct(1, 'Tea', 100), $billAcceptor);
        
        $this->assertSame(1, $product->id);
    }
    
    public function testIssueLarge()
    {
        $strategy = new \Application\Classes\CashBackStrategy\IssueLarge();
        
        $this->assertSame([5], $strategy->toIssue([1, 2, 5, 10], 5));
        
        $this->assertSame([5, 2, 2], $strategy->toIssue([1, 2, 5], 9));
        
        $this->assertSame([5, 2, 1], $strategy->toIssue([1, 2, 5], 8));
        
        $this->assertSame([500, 100, 100, 10, 10, 10, 10, 10, 10, 10, 2], $strategy->toIssue([1, 2, 5, 10, 100, 500], 772));
        
        $this->assertSame([500, 100, 100, 50, 10, 10, 2], $strategy->toIssue([1, 2, 5, 10, 50, 100, 500], 772));
        
        $this->assertSame([100, 100, 100, 100, 100, 50, 10, 10, 2], $strategy->toIssue([1, 2, 5, 10, 50, 100], 572));
    }
    
    public function testCashBack()
    {
        $coins = [1, 2, 5, 10, 50, 100, 500, 1000];
    
        $billAcceptor = new BillAcceptor($coins);
        $billAcceptor->add(1000);
        
        $coffeeMachine = new CoffeeMachine([
            $this->createProduct(1, 'Tea', 100),
            $this->createProduct(1, 'Coffee', 200)
        ]);
        
        $product = $coffeeMachine->toCook($this->createProduct(1, 'Tea', 100), $billAcceptor);
        
        $cashbox = new Cashbox($coins);
        
        $stragegy = new \Application\Classes\CashBackStrategy\IssueLarge();
        
        $this->assertSame([500, 100, 100, 100, 100], $cashbox->pay($product, $billAcceptor, $stragegy));
    }
    
    private function createProduct(int $id, string $title, int $price)
    {
        $product = new \Application\Model\Product();
        $product->exchangeArray([
            'id' => $id,
            'title' => $title,
            'price' => $price
        ]);
        return $product;
    }
}