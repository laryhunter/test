﻿$(document).ready(function () {

    $.get('/api', function(response) {
        response.products.forEach(function (product) {
            $('.products').append($('<option value="' + product.id + '">' + product.title + ' (' + product.price + ')' + '</option>'))
        });
    });
    
    $('.buy').click(function() {
    
        $('.msg').html('').hide();
    
        $.post('/api', {
            id: $('.products').val(),
            money: $('.money').val().split(',')
        }, function (response) {
            var msg = '';
            if(response.success) {
                msg = 'Спасибо за покупку. ' + response.product.title + ' Сдача: ' + response.cash_back.join(',');
                $('.money').val('');
            } else {
                switch(response.code) {
                    case 100:
                        msg = 'Выбирете что нибудь';
                        break;
                    case 101:
                        msg = 'Не достаточно средств';
                        break;
                }
                
                $('.money').val(response.coins.join(','));
            }
            
            $('.msg').html(msg).show();
        });
    }); 
    

})